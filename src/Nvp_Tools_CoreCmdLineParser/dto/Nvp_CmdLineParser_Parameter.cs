﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nvp_Tools_CoreCmdLineParser.dto
{
    public class Nvp_CmdLineParser_Parameter
    {
        private string _caption;
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                if (value.StartsWith("--"))
                {
                    _caption = value;
                }
                else
                {
                    _caption = "--" + value;
                }
            }
        }

        public bool Required { get; set; }
    }
}
