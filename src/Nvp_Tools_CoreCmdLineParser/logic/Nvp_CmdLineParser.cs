﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Nvp_Tools_CoreCmdLineParser.dto;

namespace Nvp_Tools_CoreCmdLineParser.logic
{
    public class Nvp_CmdLineParser
    {
        // +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        private string[] _args { get; }
        public List<Nvp_CmdLineParser_Parameter> _parameterList { get; set; }
        public string HelpText;




        // +++ life cycle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public Nvp_CmdLineParser(string[] args)
        {
            _args = args;
        }




        // +++ parser methods +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public Dictionary<string, string> Parse()
        {
            List<string> paramList = _args.ToList();
            var dict = new Dictionary<string, string>();

            foreach (var p in _parameterList)
            {
                if (dict.ContainsKey(p.Caption)) continue;

                var index = paramList.IndexOf(p.Caption) + 1;

                if (p.Caption.StartsWith("--"))
                {
                    dict.Add(p.Caption.Replace("--", ""), paramList[index]);
                }


            }

            return dict;
        }

        public void SetParameterList(List<Nvp_CmdLineParser_Parameter> list)
        {
            _parameterList = list;
        }

    }

}
