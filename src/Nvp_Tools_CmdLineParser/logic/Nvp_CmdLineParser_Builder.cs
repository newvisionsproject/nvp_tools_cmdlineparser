﻿using Nvp_Tools_CmdLineParser.dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nvp_Tools_CmdLineParser.logic
{
    public class Nvp_CmdLineParser_Builder
    {
        // +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        string[] _args;
        Nvp_CmdLineParser _parser;
        List<Nvp_CmdLineParser_Parameter> _parameterList;
        private string _helpText;




        // +++ life cycle +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public Nvp_CmdLineParser_Builder(string[] args)
        {
            _args = args;
            _parser = new Nvp_CmdLineParser(args);
        }

        public Nvp_CmdLineParser Build()
        {
            _parser = new Nvp_CmdLineParser(_args);
            _parser.SetParameterList(_parameterList);
            if (_parameterList == null || _parameterList.Count == 0)
            {
                Console.WriteLine(_helpText);
                throw new ApplicationException(_helpText);
            }
            else
            {
                _parser = new Nvp_CmdLineParser(_args);
                _parser.SetParameterList(_parameterList);
                return _parser;
            }
        }

        public Nvp_CmdLineParser_Builder AddParamter(Nvp_CmdLineParser_Parameter nvp_CmdLineParser_Parameter)
        {
            if(nvp_CmdLineParser_Parameter.Caption == string.Empty || nvp_CmdLineParser_Parameter.Caption == "--")
            {
                throw new ArgumentException($"Parameter must not be empty.\n{_helpText}");
            }

            List<string> paramList = _args.ToList();
            var longNameIndex = paramList.FindIndex(0, x => x == nvp_CmdLineParser_Parameter.Caption);
            if (longNameIndex < 0)
            {
                throw new ArgumentException($"Parameter {nvp_CmdLineParser_Parameter.Caption} does not exist in given arguments.");
            }
            else if (longNameIndex == paramList.Count - 1)

            {
                throw new ArgumentException($"Value for Parameter {nvp_CmdLineParser_Parameter.Caption} does not exist in given arguments.");
            }

            if (_parameterList == null) _parameterList = new List<Nvp_CmdLineParser_Parameter>();
            this._parameterList.Add(nvp_CmdLineParser_Parameter);
            return this;
        }

        public Nvp_CmdLineParser_Builder SetHelpText(string helpTex)
        {
            _helpText = helpTex;
            return this;
        }
    }

}
