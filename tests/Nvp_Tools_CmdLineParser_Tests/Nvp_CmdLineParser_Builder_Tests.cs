using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class Nvp_CmdLineParser_Builder_Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void When_build_called_and_no_parameters_configurerd_throws_Exeption_With_Help_Text()
        {
            string[] args = new string[]
            {
                "-p",
                "\\D:\tmp",
            };

            var helpText = "usage dotnet HistoryRestorer -d { PATHNAME}";


            var builder = new Nvp_Tools_CmdLineParser.logic.Nvp_CmdLineParser_Builder(args);

            System.ApplicationException ex = Assert.Throws<System.ApplicationException>(
                () =>
                {
                    var parser = builder
                                .SetHelpText(helpText)
                                .Build();
                }
                );
            Assert.That(ex.Message, Is.EqualTo(helpText));
        }

        [Test]
        public void When_parameter_configured_as_required_and_not_present_in_args_parser_thows_exeption()
        {
            string[] args = new string[]
            {
                "--path",
                "\\D:\tmp",
            };

            var helpText = "usage dotnet HistoryRestorer -d { PATHNAME}";

            var builder = new Nvp_Tools_CmdLineParser.logic.Nvp_CmdLineParser_Builder(args);

            System.ArgumentException ex = Assert.Throws<System.ArgumentException>(
                () =>
                {
                    var parser = builder
                                .SetHelpText(helpText)
                                .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "", Required=true})
                                .Build();
                }
                );
            Assert.That(ex.Message, Is.EqualTo($"Parameter must not be empty.\n{helpText}"));
        }

        [Test]
        public void When_given_arguments_did_not_contain_value_for_parameter()
        {
            string[] args = new string[]
            {
                "--path",
            };

            var helpText = "usage dotnet HistoryRestorer -d { PATHNAME}";

            var builder = new Nvp_Tools_CmdLineParser.logic.Nvp_CmdLineParser_Builder(args);
            System.ArgumentException ex = Assert.Throws<System.ArgumentException>(
                () =>
                {
                    var parser = builder
                                .SetHelpText(helpText)
                                .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "--path", Required = true })
                                .Build();
                }
                );

            Assert.That(ex.Message, Is.EqualTo($"Value for Parameter --path does not exist in given arguments."));            
        }

        [Test]
        public void With_given_paramater_definition_parser_is_able_to_get_dictionary_from_argument_list()
        {
            string[] args = new string[]
            {
                "--path",
                "d:\\tmp\\"
            };

            var helpText = "usage dotnet HistoryRestorer -d { PATHNAME}";


            var builder = new Nvp_Tools_CmdLineParser.logic.Nvp_CmdLineParser_Builder(args);

            var parser = builder
                        .SetHelpText(helpText)
                        .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "--path", Required = true })
                        .Build();

            Dictionary<string, string> parameterValues = parser.Parse();

            Assert.That(parameterValues.ContainsKey("path"), Is.True);
            Assert.That(parameterValues["path"], Is.EqualTo("d:\\tmp\\"));
        }

        [Test]
        public void With_given_multiple_paramater_definitions_parser_is_able_to_get_dictionary_from_argument_list()
        {
            string[] args = new string[]
            {
                "--path",
                "d:\\tmp\\",
                "--value",
                "true",
                "--digit",
                "42"
            };

            var helpText = "usage dotnet HistoryRestorer -d { PATHNAME}";


            var builder = new Nvp_Tools_CmdLineParser.logic.Nvp_CmdLineParser_Builder(args);

            var parser = builder
                        .SetHelpText(helpText)
                        .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "path", Required = true })
                        .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "digit", Required = true })
                        .AddParamter(new Nvp_Tools_CmdLineParser.dto.Nvp_CmdLineParser_Parameter() { Caption = "value", Required = true })
                        .Build();

            Dictionary<string, string> parameterValues = parser.Parse();

            Assert.That(parameterValues.ContainsKey("path"), Is.True);
            Assert.That(parameterValues["path"], Is.EqualTo("d:\\tmp\\"));
            Assert.That(parameterValues.ContainsKey("digit"), Is.True);
            Assert.That(parameterValues["digit"], Is.EqualTo("42"));
            Assert.That(parameterValues.ContainsKey("value"), Is.True);
            Assert.That(parameterValues["value"], Is.EqualTo("true"));
        }
    }
}